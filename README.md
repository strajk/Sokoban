Sokoban project
Author: Artur Gałuszka

https://gitlab.com/strajk/Sokoban

This is a puzzle game made for "Design Patterns" subject in my 2nd year of university.
It uses OpenGL graphics.

running using jar(recommended):
go to /executable directory
run using console:
java -Djava.library.path=windows -jar Sokoban.jar
OR
java -Djava.library.path=linux -jar Sokoban.jar

running using code:
all needed external libraries are in /jar directory
VM options: -Djava.library.path=native/windows (or linux)