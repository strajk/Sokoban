package levelLoading;

import control.GameSettings;
import gameWorld.*;
import gameWorld.logicalObjects.Box;
import gameWorld.logicalObjects.Cross;
import gameWorld.logicalObjects.Player;
import gameWorld.logicalObjects.Wall;
import math.Vec2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by strajk on 11.06.15.
 */
public class LevelLoader {
    JSONParser jsonParser;

    public LevelLoader() {
        this.jsonParser = new JSONParser();
    }

    public Level loadLevel(int levelNumber) {
        LoadedLevel levelToParse = jsonParser.getLoadedLevel(levelNumber);
        return levelParser(levelToParse);
    }

    private Level levelParser(LoadedLevel levelToParse) {
        Player player = null;
        List<Wall> wallList = new ArrayList<Wall>();
        List<Cross> crossList = new ArrayList<Cross>();
        List<Box> boxList = new ArrayList<Box>();
        for (LoadedLevel.LevelData data : levelToParse.data) {
            switch (data.getType()) {
                case CROSS:
                    crossList.add(new Cross(new Vec2(data.x, data.y), new Vec2(GameSettings.ELEMENTS_SIZE, GameSettings.ELEMENTS_SIZE)));
                    break;
                case BOX:
                    boxList.add(new Box(new Vec2(data.x, data.y), new Vec2(GameSettings.ELEMENTS_SIZE, GameSettings.ELEMENTS_SIZE)));
                    break;
                case WALL:
                    wallList.add(new Wall(new Vec2(data.x, data.y), new Vec2(GameSettings.ELEMENTS_SIZE, GameSettings.ELEMENTS_SIZE)));
                    break;
                case PLAYER:
                    player = new Player(new Vec2(data.x, data.y), new Vec2(GameSettings.ELEMENTS_SIZE, GameSettings.ELEMENTS_SIZE));
                    break;
            }
        }
        Level level = new Level();
        level.setBoxList(boxList);
        level.setCrossList(crossList);
        level.setWallList(wallList);
        level.setPlayer(player);
        return level;
    }

}
