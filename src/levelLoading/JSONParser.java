package levelLoading;

import com.fasterxml.jackson.databind.ObjectMapper;
import control.GameSettings;

import java.io.File;
import java.io.IOException;

/**
 * Created by strajk on 10.06.15.
 */
public class JSONParser {

    ObjectMapper mapper;
    public JSONParser(){
        this.mapper=new ObjectMapper();
    }
    public LoadedLevel getLoadedLevel(int levelNumber) {
        LoadedLevel level= null;
        try {
            level = mapper.readValue(new File(GameSettings.LEVEL_PATH+levelNumber+".json"), LoadedLevel.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return level;
    }

/* TO DO  loading levels from files: 'TYPE' (enum),'POSITION' (integers); positions in files are in game numbers :e.x.
* Box is in position 2,3 ;colours,textures,sizes in cfg file(singleton); change graphical and logical coordinates to game type
* : from Box (20,30) to Box(2,3), or create math.coordinatesParser;loading graphical cfg from file?*/
}
