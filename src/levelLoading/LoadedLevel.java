package levelLoading;

import java.util.List;

/**
 * Created by strajk on 12.06.15.
 */
enum Type {
    BOX, PLAYER, WALL, CROSS
}

public class LoadedLevel {

    public static class LevelData {

        public Type type;
        public int x;
        public int y;

        public LevelData() {
        }

        public Type getType() {
            return type;
        }

        public void setType(Type type) {
            this.type = type;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }

    public List<LevelData> data;

}
