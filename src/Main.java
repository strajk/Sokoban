import control.ElementsController;
import control.GameSettings;
import control.Steering;
import gameWorld.GameEngine;
import graphics.DisplayController;
import graphics.Window;
import org.lwjgl.opengl.Display;

import java.util.ArrayList;
import java.util.List;

public class Main {
    graphics.Window window = new Window();

    DisplayController displayController;
    GameEngine gameEngine = new GameEngine();
    Steering steering = new Steering(gameEngine, window);
    ElementsController elementsController;

    public static void main(String[] args) {
        Main game = new Main();
        game.run();
    }

    public void run() {
        window.createWindow();
        window.getDelta();
        window.initGL();
        window.initWorld();
        displayController = new DisplayController();
        elementsController = new ElementsController(gameEngine, displayController);
        //elementsController.getObjects();
        elementsController.updateBackgrouond();
        while (!window.closeRequested) {
            float delta = 1.0F / GameSettings.MAX_FPS;
            steering.pollInput(delta);
            //
            gameEngine.update();
            elementsController.getObjects();

            displayController.setCameraReach(gameEngine.furthestWallCoordinate());
            List<String> text = new ArrayList<String>();
            text.add("LEVEL: " + gameEngine.levelNumber);
            text.add("R - level restart");
            text.add("Z - undo move");

            displayController.setTextDisplay(text);
            //
            displayController.renderGL();
            Display.update();
        }
        window.cleanup();

    }

}
