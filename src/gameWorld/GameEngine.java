package gameWorld;

import commands.LevelObserver;
import gameWorld.logicalObjects.Box;
import gameWorld.logicalObjects.Cross;
import gameWorld.logicalObjects.WorldObject;
import levelLoading.LevelLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by strajk on 11.06.15.
 */
public class GameEngine {
    public GameWorld gameWorld;
    public LevelLoader levelLoader;
    public int levelNumber;
    public int score;
    List<LevelObserver> levelObservers;

    public GameEngine() {
        this.score = 0;
        this.gameWorld = new GameWorld();
        this.levelNumber = 1;
        this.levelLoader = new LevelLoader();
        levelObservers = new ArrayList<LevelObserver>();
        gameWorld.loadLevel(levelLoader.loadLevel(levelNumber));
    }

    public void update() {
        gameWorld.update();
        if (checkCrossFilling()) {
            this.levelNumber++;
            gameWorld.loadLevel(levelLoader.loadLevel(levelNumber));
            for (LevelObserver observer : levelObservers) {
                observer.levelCompleted();
            }
        }
    }

    public void levelRestart() {
        gameWorld.loadLevel(levelLoader.loadLevel(levelNumber));
    }

    private boolean checkCrossFilling() {
        boolean allFilled = true;
        for (Cross cross : gameWorld.crosses) {
            cross.filled = false;
            for (Box box : gameWorld.boxes) {
                if (cross.position.equals(box.position)) {
                    cross.filled = true;
                    break;
                }
            }
        }
        for (Cross cross : gameWorld.crosses) {
            if (!cross.filled) {
                allFilled = false;
            }
        }
        return allFilled;
    }

    public List<WorldObject> getObjects() {
        return gameWorld.getObjects();
    }

    public float furthestWallCoordinate() {
        return gameWorld.furthestWallCoordinate();
    }

    public void addLevelObserver(LevelObserver levelObserver) {
        this.levelObservers.add(levelObserver);
    }
}
