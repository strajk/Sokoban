package gameWorld;

import gameWorld.logicalObjects.*;
import math.Vec2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by strajk on 08.06.15.
 */
public class GameWorld extends WorldObject {
    public Player player;
    List<Box> boxes;
    List<Cross> crosses;
    List<Wall> walls;

    public GameWorld() {

    }

    @Override
    public List<WorldObject> getObjects() {
        List<WorldObject> list = new ArrayList<WorldObject>();
        for (Wall wall : walls) {
            list.add(wall);
        }
        for (Box box : boxes) {
            list.addAll(box.getObjects());
        }
        for (Cross cross : crosses) {
            list.addAll(cross.getObjects());
        }
        list.add(player);
        return list;
    }

    public void movePlayer(Vec2 vector) {
        if (canPlayerMove(vector)) {
            Box boxToMove = boxToMove(vector);
            if (boxToMove != null) {
                boxToMove.move(vector);
            }
            player.move(vector);
        }
    }

    public void update() {
        player.update();
        for (Box box : boxes) {
            box.update();
        }
    }

    public void loadLevel(Level level) {
        this.walls = level.wallList;
        this.crosses = level.crossList;
        this.boxes = level.boxList;
        this.player = level.player;
        player.setGameWorld(this);
    }

    private boolean canBoxMove(Box box, Vec2 vector) {
        for (Box collidingBox : boxes) {
            if (collidingBox != box) {
                Vec2 newPossiblePosition = new Vec2(box.position);
                newPossiblePosition.addLocal(vector);
                if (newPossiblePosition.equals(collidingBox.position)) {
                    return false;
                }
            }
        }
        for (Wall collidingWall : walls) {
            Vec2 newPossiblePosition = new Vec2(box.position);
            newPossiblePosition.addLocal(vector);
            if (newPossiblePosition.equals(collidingWall.position)) {
                return false;
            }
        }
        return true;
    }

    public boolean canPlayerMove(Vec2 vector) {
        if (!player.movementSpeed.equals(new Vec2(0.0f, 0.0f))) {
            return false;
        }
        for (Wall collidingWall : walls) {
            Vec2 newPossiblePosition = new Vec2(player.position);
            newPossiblePosition.addLocal(vector);
            if (newPossiblePosition.equals(collidingWall.position)) {
                return false;
            }
        }
        for (Box collidingBox : boxes) {
            Vec2 newPossiblePosition = new Vec2(player.position);
            newPossiblePosition.addLocal(vector);
            if (newPossiblePosition.equals(collidingBox.position)) {
                return canBoxMove(collidingBox, vector);
            }
        }
        return true;
    }

    public Box boxToMove(Vec2 vector) {
        Box boxToMove = null;
        for (Box collidingBox : boxes) {
            Vec2 newPossiblePosition = new Vec2(player.position);
            newPossiblePosition.addLocal(vector);
            if (newPossiblePosition.equals(collidingBox.position)) {
                boxToMove = collidingBox;
            }
        }
        return boxToMove;
    }

    public float furthestWallCoordinate() {
        float coordinate = 0.0f;
        for (Wall wall : walls) {
            if (wall.position.x > coordinate) {
                coordinate = wall.position.x;
            }
            if (wall.position.y > coordinate) {
                coordinate = wall.position.y;
            }
        }
        return coordinate;
    }
}
