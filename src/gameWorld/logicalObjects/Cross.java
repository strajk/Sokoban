package gameWorld.logicalObjects;

import math.Vec2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by strajk on 10.06.15.
 */
public class Cross extends WorldObject {
    public boolean filled;

    public Cross(Vec2 position, Vec2 size) {
        this.position = position;
        this.size = size;
        this.filled = false;
    }

    @Override
    public List<WorldObject> getObjects() {
        List<WorldObject> list = new ArrayList<WorldObject>();
        list.add(this);
        return list;
    }
}
