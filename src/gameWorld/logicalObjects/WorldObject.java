package gameWorld.logicalObjects;

import math.Vec2;

import java.util.List;

/**
 * Created by strajk on 08.06.15.
 */
public abstract class WorldObject {
    public Vec2 position;
    public Vec2 size;

    public abstract List<WorldObject> getObjects();

}
