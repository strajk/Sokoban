package gameWorld.logicalObjects;

import gameWorld.GameWorld;
import math.Vec2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by strajk on 09.06.15.
 */
public class Player extends WorldObject {

    public Vec2 finalPosition;
    public Vec2 movementSpeed;
    public GameWorld gameWorld;

    public Player(Vec2 position, Vec2 size) {
        this.position = position;
        this.size = size;
        this.movementSpeed = new Vec2(0, 0);
        this.finalPosition = new Vec2(position);
    }

    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    @Override
    public List<WorldObject> getObjects() {
        List<WorldObject> list = new ArrayList<WorldObject>();
        list.add(this);
        return list;
    }

    public void move(Vec2 vector) {
        this.finalPosition.addLocal(vector);
        this.movementSpeed = new Vec2(vector.x / 20, vector.y / 20);
    }

    public void update() {
        position.addLocal(movementSpeed);
        if (position.equals(finalPosition)) {
            movementSpeed = new Vec2(0, 0);
        }
    }
}
