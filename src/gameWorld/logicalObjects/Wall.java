package gameWorld.logicalObjects;

import math.Vec2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by strajk on 09.06.15.
 */
public class Wall extends WorldObject {
    public Wall(Vec2 position, Vec2 size) {
        this.position = position;
        this.size = size;
    }

    @Override
    public List<WorldObject> getObjects() {
        List<WorldObject> list = new ArrayList<WorldObject>();
        list.add(this);
        return list;
    }
}
