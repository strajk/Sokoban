package gameWorld;

import gameWorld.logicalObjects.Box;
import gameWorld.logicalObjects.Cross;
import gameWorld.logicalObjects.Player;
import gameWorld.logicalObjects.Wall;

import java.util.List;

/**
 * Created by strajk on 10.06.15.
 */
public class Level {
    Player player;
    List<Wall> wallList;
    List<Cross> crossList;
    List<Box> boxList;

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setWallList(List<Wall> wallList) {
        this.wallList = wallList;
    }

    public void setCrossList(List<Cross> crossList) {
        this.crossList = crossList;
    }

    public void setBoxList(List<Box> boxList) {
        this.boxList = boxList;
    }


    public Level() {
    }


}
