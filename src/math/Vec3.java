package math;

/**
 * Created by strajk on 27.05.15.
 */
public class Vec3 {

    public Vec3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float x;
    public float y;
    public float z;

    public void addLocal(Vec3 vec){
        this.x += vec.x;
        this.y += vec.y;
        this.z += vec.z;
    }
}
