package math;

/**
 * Created by strajk on 06.06.15.
 */
public class Vec2 {
    private static float ERROR = 0.001f;

    public Vec2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vec2(Vec2 vec) {
        this.x = vec.x;
        this.y = vec.y;
    }

    public float x;
    public float y;

    public void addLocal(Vec2 vec) {
        this.x += vec.x;
        this.y += vec.y;
    }

    public Vec2 add(Vec2 vec) {
        return new Vec2(this.x + vec.x, this.y + vec.y);
    }

    public boolean isZero() {
        if (floatEqual(this.x, 0.0f) && floatEqual(this.y, 0.0f)) {
            return true;
        }
        return false;
    }

    public boolean equals(Vec2 vec) {
        if (floatEqual(this.x, vec.x) && floatEqual(this.y, vec.y)) {
            return true;
        }
        return false;
    }

    private boolean floatEqual(float a, float b) {
        if (a - b < ERROR && a - b > (-ERROR)) {
            return true;
        }
        return false;
    }
}