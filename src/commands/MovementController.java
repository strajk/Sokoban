package commands;

import java.util.Stack;

/**
 * Created by strajk on 13.06.15.
 */
public class MovementController implements LevelObserver {
    private Stack<UndoableCommand> commandStack;

    public MovementController() {
        commandStack = new Stack<UndoableCommand>();
    }

    public void execute(UndoableCommand command) {
        if (command.execute()) {
            commandStack.add(command);
        }
    }

    public void undo() {
        if (!commandStack.isEmpty()) {
            commandStack.pop().undo();
        }
    }

    public void reset() {
        commandStack = new Stack<UndoableCommand>();
    }

    @Override
    public void levelCompleted() {
        reset();
    }
}
