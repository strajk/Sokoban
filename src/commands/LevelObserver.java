package commands;

/**
 * Created by strajk on 13.06.15.
 */
public interface LevelObserver {
    public void levelCompleted();
}
