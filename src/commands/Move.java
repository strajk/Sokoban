package commands;

import gameWorld.logicalObjects.Box;
import gameWorld.GameWorld;
import math.Vec2;

/**
 * Created by strajk on 13.06.15.
 */
public class Move extends UndoableCommand {
    Vec2 vector;

    Box boxToMove = null;
    boolean completedExecution = false;

    public Move(GameWorld gameWorld, Vec2 vector) {
        this.gameWorld = gameWorld;
        this.vector = vector;
    }

    @Override
    public boolean execute() {
        if (gameWorld.canPlayerMove(vector)) {
            boxToMove = gameWorld.boxToMove(vector);
            if (boxToMove != null) {
                boxToMove.move(vector);
            }
            gameWorld.player.move(vector);
            this.completedExecution = true;
        }
        return completedExecution;
    }

    @Override
    public void undo() {
        if (completedExecution) {
            Vec2 undoVector = new Vec2(-vector.x, -vector.y);
            if (boxToMove != null) {
                boxToMove.move(undoVector);
            }
            gameWorld.player.move(undoVector);
        }
    }
}
