package commands;

import gameWorld.GameWorld;

/**
 * Created by strajk on 13.06.15.
 */
public abstract class UndoableCommand {
    GameWorld gameWorld;

    public abstract boolean execute();

    public abstract void undo();
}
