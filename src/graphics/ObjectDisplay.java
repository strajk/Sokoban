package graphics;

import control.GameSettings;
import graphics.graphicalObjects.*;
import math.Vec2;
import math.Vec3;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by strajk on 09.06.15.
 */
public class ObjectDisplay {
    Map<String, Texture> textureMap;

    public ObjectDisplay() {
        this.textureMap = new HashMap<String, Texture>();
        try {
            Texture texture;
            texture = TextureLoader.getTexture("PNG", new FileInputStream(GameSettings.TEXTURES_PATH + "box.png"));
            textureMap.put("box", texture);
            texture = TextureLoader.getTexture("PNG", new FileInputStream(GameSettings.TEXTURES_PATH + "player.png"));
            textureMap.put("player", texture);
            texture = TextureLoader.getTexture("PNG", new FileInputStream(GameSettings.TEXTURES_PATH + "wall.png"));
            textureMap.put("wall", texture);
            texture = TextureLoader.getTexture("PNG", new FileInputStream(GameSettings.TEXTURES_PATH + "background.png"));
            textureMap.put("background", texture);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void display(WorldObjectGraphics object) {
        if (object instanceof BoxGraphics) {
            display((BoxGraphics) object);
        } else if (object instanceof PlayerGraphics) {
            display((PlayerGraphics) object);
        } else if (object instanceof WallGraphics) {
            display((WallGraphics) object);
        } else if (object instanceof CrossGraphics) {
            display((CrossGraphics) object);
        } else if (object instanceof BackGroundSquare) {
            display((BackGroundSquare) object);
        }
    }

    public void display(BoxGraphics box) {
        //DisplayUtils.drawRectangle(box.position,0,box.size,new Vec3(0.4f,0.4f,0.4f));
        DisplayUtils.drawRectangleTextured(box.position, 0, new Vec2(GameSettings.ELEMENTS_SIZE, GameSettings.ELEMENTS_SIZE), new Vec3(0.4f, 0.4f, 0.4f), textureMap.get("box"));
    }

    public void display(PlayerGraphics player) {
        DisplayUtils.drawRectangleTextured(player.position, 0, new Vec2(GameSettings.ELEMENTS_SIZE, GameSettings.ELEMENTS_SIZE), new Vec3(0.8f, 085f, 0.85f), textureMap.get("player"));
    }

    public void display(WallGraphics wall) {
        DisplayUtils.drawRectangleTextured(wall.position, 0, new Vec2(GameSettings.ELEMENTS_SIZE, GameSettings.ELEMENTS_SIZE), new Vec3(0.2f, 0.65f, 0.65f), textureMap.get("wall"));
    }

    public void display(CrossGraphics cross) {
        if (cross.covered == false) {
            DisplayUtils.drawRectangle(cross.position, 45, new Vec2(GameSettings.ELEMENTS_SIZE * 0.8f, GameSettings.ELEMENTS_SIZE * 0.2f), new Vec3(0.8f, 0.2f, 0.2f));
            DisplayUtils.drawRectangle(cross.position, 135, new Vec2(GameSettings.ELEMENTS_SIZE * 0.8f, GameSettings.ELEMENTS_SIZE * 0.2f), new Vec3(0.8f, 0.2f, 0.2f));
        } else {
            DisplayUtils.drawRectangle(cross.position, 45, new Vec2(GameSettings.ELEMENTS_SIZE * 0.8f, GameSettings.ELEMENTS_SIZE * 0.2f), new Vec3(0.2f, 0.8f, 0.2f));
            DisplayUtils.drawRectangle(cross.position, 135, new Vec2(GameSettings.ELEMENTS_SIZE * 0.8f, GameSettings.ELEMENTS_SIZE * 0.2f), new Vec3(0.2f, 0.8f, 0.2f));
        }
    }

    public void display(BackGroundSquare square) {
        DisplayUtils.drawRectangleTextured(square.position, 0, new Vec2(GameSettings.ELEMENTS_SIZE, GameSettings.ELEMENTS_SIZE), new Vec3(0.2f, 0.65f, 0.65f), textureMap.get("background"));
    }
}
