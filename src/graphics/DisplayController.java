package graphics;

import graphics.graphicalObjects.BackGroundSquare;
import graphics.graphicalObjects.WorldObjectGraphics;
import math.Vec2;
import math.Vec3;
import org.lwjgl.util.glu.GLU;


import java.util.List;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by strajk on 09.06.15.
 */
public class DisplayController {
    List<WorldObjectGraphics> list;
    List<BackGroundSquare> background;
    ObjectDisplay objectDisplay;
    Camera camera;
    DisplayUtils utils;
    TextDisplay textDisplay;
    float cameraHeight;
    List<String> textToPrint;

    public DisplayController() {
        cameraHeight = 10.0f;
        camera = new Camera(new Vec3(0.0f, 0.0f, 10));
        this.objectDisplay = new ObjectDisplay();
        this.utils = new DisplayUtils();
        textDisplay = TextDisplay.getInstance();
    }

    public void setObjects(List<WorldObjectGraphics> list) {
        this.list = list;
    }

    public void display() {
        for (BackGroundSquare square : background) {
            objectDisplay.display(square);
        }
        for (WorldObjectGraphics object : list) {
            objectDisplay.display(object);
        }
    }

    public void renderGL() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer

        utils.setPerspectiveView();
        //utils.drawGrid();
        display(); // Draw all Objects
        glLoadIdentity(); // Reset The View

        Vec3 camPosition = camera.getPosition();
        GLU.gluLookAt(camPosition.x, camPosition.y, camPosition.z, camPosition.x, camPosition.y, camPosition.z - cameraHeight, 0.0f, 1.0f, 0.0f);

        utils.setOrthographicView();

        textDisplay.printText(textToPrint);
    }

    public void setCameraReach(float maxPosition) {
        cameraHeight = 1.2071f * (maxPosition + 1);
        camera.setCameraPosition(new Vec2(maxPosition / 2, maxPosition / 2), cameraHeight);
    }

    public void setTextDisplay(List<String> textToPrint) {
        this.textToPrint = textToPrint;
    }

    public void setBackground(List<BackGroundSquare> squares) {
        this.background = squares;
    }
}
