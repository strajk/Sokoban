package graphics;

import control.GameSettings;
import math.Vec2;
import math.Vec3;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glPopMatrix;

/**
 * Created by strajk on 09.06.15.
 */
public class DisplayUtils {
    public void setPerspectiveView() {
        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glMatrixMode(GL11.GL_PROJECTION);                        // Select The Projection Matrix
        GL11.glPopMatrix();                                      // Restore The Old Projection Matrix
        GL11.glMatrixMode(GL11.GL_MODELVIEW);                         // Select The Modelview Matrix
        GL11.glPopMatrix();                                      // Restore The Old Projection Matrix

        // Following lines enable draw order
        //GL11.glEnable(GL11.GL_DEPTH_TEST);
        glDepthFunc(GL_NEVER);      // Ignore depth values (Z) to cause drawing bottom to top
    }

    public void setOrthographicView() {
        GL11.glMatrixMode(GL11.GL_PROJECTION);                        // Select The Projection Matrix
        GL11.glPushMatrix();                                     // Store The Projection Matrix
        GL11.glLoadIdentity();                                   // Reset The Projection Matrix
        GL11.glOrtho(0, GameSettings.WIDTH, 0, GameSettings.HEIGHT, 1, -1);                          // Set Up An Ortho Screen
        GL11.glMatrixMode(GL11.GL_MODELVIEW);                         // Select The Modelview Matrix
        GL11.glPushMatrix();                                     // Store The Modelview Matrix
        GL11.glLoadIdentity();// Reset The Modelview Matrix          // Enable Depth Mask
    }

    public static void drawGrid() {
        int gridSize = 100;
        float squareSize = GameSettings.ELEMENTS_SIZE;

        GL11.glBegin(GL11.GL_LINES);
        for (int i = 0; i < gridSize; i++) {
            GL11.glColor3f(0.85f, 0.85f, 0.85f);

            GL11.glVertex3f(-squareSize * gridSize / 2 + (i + 0.5f) * squareSize, -squareSize * gridSize / 2, -0.01f);
            GL11.glVertex3f(-squareSize * gridSize / 2 + (i + 0.5f) * squareSize, squareSize * gridSize / 2, -0.01f);

            GL11.glVertex3f(-squareSize * gridSize / 2, -squareSize * gridSize / 2 + (i + 0.5f) * squareSize, -0.01f);
            GL11.glVertex3f(squareSize * gridSize / 2, -squareSize * gridSize / 2 + (i + 0.5f) * squareSize, -0.01f);
        }
        GL11.glEnd();
    }

    public static void drawRectangle(Vec2 position, float angle, Vec2 size, Vec3 color) {
        glPushMatrix();
        glTranslatef(position.x, position.y, 0.0f);
        glRotatef(angle, 0, 0, 1.0f);
        glBegin(GL_QUADS);

        glColor3f(color.x, color.y, color.z);
        glVertex3f(size.x / 2, -size.y / 2, 0.0f);
        glVertex3f(size.x / 2, size.y / 2, 0.0f);
        glVertex3f(-size.x / 2, size.y / 2, 0.0f);
        glVertex3f(-size.x / 2, -size.y / 2, 0.0f);
        glEnd();
        glPopMatrix();
    }

    public static void drawRectangleTextured(Vec2 position, float angle, Vec2 size, Vec3 color, Texture texture) {
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        glPushMatrix();
        glTranslatef(position.x, position.y, 0.0f);
        glRotatef(angle, 0, 0, 1.0f);
        texture.bind();
        glBegin(GL_QUADS);

        glColor3f(color.x, color.y, color.z);
        GL11.glTexCoord2f(1.0f, 1.0f);
        glVertex3f(size.x / 2, -size.y / 2, 0.0f);
        GL11.glTexCoord2f(1.0f, 0.0f);
        glVertex3f(size.x / 2, size.y / 2, 0.0f);
        GL11.glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-size.x / 2, size.y / 2, 0.0f);
        GL11.glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-size.x / 2, -size.y / 2, 0.0f);
        glEnd();
        glPopMatrix();
        GL11.glDisable(GL11.GL_TEXTURE_2D);
    }

}
