package graphics.graphicalObjects;

import math.Vec2;

/**
 * Created by strajk on 13.06.15.
 */
public class CrossGraphics extends WorldObjectGraphics {
    public boolean covered = false;

    public CrossGraphics(Vec2 position, boolean covered) {
        this.covered = covered;
        this.position = position;
    }
}
