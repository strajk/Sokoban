package graphics.graphicalObjects;

import math.Vec2;

/**
 * Created by strajk on 13.06.15.
 */
public abstract class WorldObjectGraphics {
    public Vec2 position;
    public Vec2 size;
}
