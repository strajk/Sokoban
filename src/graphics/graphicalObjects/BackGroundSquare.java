package graphics.graphicalObjects;

import math.Vec2;

/**
 * Created by strajk on 13.06.15.
 */
public class BackGroundSquare extends WorldObjectGraphics {
    public boolean visited;

    public BackGroundSquare(Vec2 position) {
        this.position = position;
        this.visited = false;
    }

    public boolean equals(Object object) {
        boolean same = false;
        if (object != null && object instanceof BackGroundSquare) {
            if (this.position.equals(((BackGroundSquare) object).position)) {
                same = true;
            }
        }
        return same;
    }
}