package graphics;

import control.GameSettings;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

/**
 * Created by strajk on 23.05.15.
 */
public class Window {
    //Steering steering;
    String windowTitle = "Sokoban";
    TextDisplay textDisplay;

    public boolean closeRequested = false;

    long lastFrameTime; // used to calculate delta

    public Window() {
        //steering=new Steering();
        textDisplay = TextDisplay.getInstance();
    }

    public void createWindow() {
        try {
            Display.setDisplayMode(new DisplayMode(GameSettings.WIDTH, GameSettings.HEIGHT));
            Display.setVSyncEnabled(true);
            Display.sync(GameSettings.MAX_FPS);
            Display.setTitle(windowTitle);
            Display.setFullscreen(false);
            Display.create();

        } catch (LWJGLException e) {
            Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
            System.exit(0);
        }
    }

    public int getDelta() {
        long time = (Sys.getTime() * 1000) / Sys.getTimerResolution();
        int delta = (int) (time - lastFrameTime);
        lastFrameTime = time;
        return delta;
    }

    public void initGL() {
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glEnable(GL11.GL_TEXTURE_2D); // Enable Texture Mapping
        GL11.glClearColor(0.7f, 0.7f, 0.7f, 0.7f); // Black Background
        GL11.glDisable(GL11.GL_DITHER);
        GL11.glDepthFunc(GL11.GL_LESS); // Depth function less or equal
        GL11.glEnable(GL11.GL_NORMALIZE); // calculated normals when scaling
        GL11.glEnable(GL11.GL_CULL_FACE); // prevent render of back surface
        GL11.glEnable(GL11.GL_BLEND); // Enabled blending
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA); // selects blending method
        GL11.glEnable(GL11.GL_ALPHA_TEST); // allows alpha channels or transperancy
        GL11.glAlphaFunc(GL11.GL_GREATER, 0.1f); // sets aplha function
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST); // High quality visuals
        GL11.glHint(GL11.GL_POLYGON_SMOOTH_HINT, GL11.GL_NICEST); //  Really Nice Perspective Calculations
        GL11.glShadeModel(GL11.GL_SMOOTH); // Enable Smooth Shading
        GL11.glViewport(0, 0, GameSettings.WIDTH, GameSettings.HEIGHT);
        GL11.glMatrixMode(GL11.GL_PROJECTION); // Select The Projection Matrix
        GL11.glLoadIdentity(); // Reset The Projection Matrix
        GLU.gluPerspective(45, GameSettings.WIDTH / (float) GameSettings.HEIGHT, 0.1f, 1000f);  //Aspect Ratio Of The graphics.Window
        GL11.glMatrixMode(GL11.GL_MODELVIEW); // Select The Modelview Matrix
        GL11.glDepthMask(true);                             // Enable Depth Mask
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        textDisplay.init();
    }

    public void initWorld() {
    }

    public void cleanup() {
        Display.destroy();
    }

    public void requestClose() {
        closeRequested = true;
    }

    public boolean getCloseRequested() {
        return closeRequested;
    }
}
