package graphics;

import math.Vec2;
import math.Vec3;

/**
 * Created by strajk on 23.05.15.
 */
public class Camera {

    private Vec3 position;

    public Camera(Vec3 position) {
        this.position = position;
    }

    public void move(Vec3 direction) {
        position.addLocal(direction);
    }


    public Vec3 getPosition() {
        return position;
    }

    public void setCameraPosition(Vec2 position, float height) {
        this.position.z = height;
        this.position.y = position.y;
        this.position.x = position.x;
    }
}
