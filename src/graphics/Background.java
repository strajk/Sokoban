package graphics;

import graphics.graphicalObjects.BackGroundSquare;
import graphics.graphicalObjects.WallGraphics;
import math.Vec2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by strajk on 13.06.15.
 */
public class Background {
    List<BackGroundSquare> backGroundSquares;
    Vec2 left;
    Vec2 up;
    Vec2 right;
    Vec2 down;

    public Background() {
        this.left = new Vec2(-1, 0);
        this.up = new Vec2(0, 1);
        this.right = new Vec2(1, 0);
        this.down = new Vec2(0, -1);
        backGroundSquares = new ArrayList<BackGroundSquare>();
    }

    public List<BackGroundSquare> getBackgroundSquares(Vec2 startingPoint, List<WallGraphics> wallList) {
        backGroundSquares = new ArrayList<BackGroundSquare>();
        Queue<BackGroundSquare> queue = new LinkedList<BackGroundSquare>();
        BackGroundSquare startingSquare = new BackGroundSquare(new Vec2(startingPoint));
        startingSquare.visited = true;
        queue.add(startingSquare);
        backGroundSquares.add(startingSquare);
        while (!queue.isEmpty()) {
            BackGroundSquare square = queue.poll();

            Vec2 newPosition = new Vec2(square.position.add(left));
            for (WallGraphics wall : wallList) {
                if (newPosition.equals(wall.position)) {
                    newPosition = null;
                    break;
                }
            }
            if (newPosition != null) {
                BackGroundSquare newSquare = new BackGroundSquare(new Vec2(newPosition));
                if (!backGroundSquares.contains(newSquare)) {
                    backGroundSquares.add(newSquare);
                    newSquare.visited = true;
                    queue.add(newSquare);
                }
            }
            newPosition = new Vec2(square.position.add(up));
            for (WallGraphics wall : wallList) {
                if (newPosition.equals(wall.position)) {
                    newPosition = null;
                    break;
                }
            }
            if (newPosition != null) {
                BackGroundSquare newSquare = new BackGroundSquare(new Vec2(newPosition));
                if (!backGroundSquares.contains(newSquare)) {
                    backGroundSquares.add(newSquare);
                    newSquare.visited = true;
                    queue.add(newSquare);
                }
            }
            newPosition = new Vec2(square.position.add(right));
            for (WallGraphics wall : wallList) {
                if (newPosition.equals(wall.position)) {
                    newPosition = null;
                    break;
                }
            }
            if (newPosition != null) {
                BackGroundSquare newSquare = new BackGroundSquare(new Vec2(newPosition));
                if (!backGroundSquares.contains(newSquare)) {
                    backGroundSquares.add(newSquare);
                    newSquare.visited = true;
                    queue.add(newSquare);
                }
            }
            newPosition = new Vec2(square.position.add(down));
            for (WallGraphics wall : wallList) {
                if (newPosition.equals(wall.position)) {
                    newPosition = null;
                    break;
                }
            }
            if (newPosition != null) {
                BackGroundSquare newSquare = new BackGroundSquare(new Vec2(newPosition));
                if (!backGroundSquares.contains(newSquare)) {
                    backGroundSquares.add(newSquare);
                    newSquare.visited = true;
                    queue.add(newSquare);
                }
            }
        }
        return backGroundSquares;
    }

}
