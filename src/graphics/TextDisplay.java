package graphics;

import control.GameSettings;
import org.lwjgl.opengl.GL11;

import java.awt.*;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glColor3f;

/**
 * Created by strajk on 12.06.15.
 */
public class TextDisplay {
    private TrueTypeFont font2;
    private static TextDisplay instance;

    public TextDisplay() {
    }

    public static TextDisplay getInstance() {
        if (instance == null) {
            instance = new TextDisplay();
        }
        return instance;
    }

    public void init() {
        Font awtFont = new Font("Arial", Font.PLAIN, 20);
        font2 = new TrueTypeFont(awtFont, true);
    }

    public void printText(java.util.List<String> lines) {
        StringBuilder strBuilder = new StringBuilder("");
        for (String string : lines) {
            strBuilder.append(string);
            strBuilder.append("\n");
        }
        GL11.glEnable(GL_TEXTURE_2D);
        glColor3f(0f, 0f, 0f);
        font2.drawString(5, GameSettings.HEIGHT - 25, strBuilder.toString(), 1f, 1f, TrueTypeFont.ALIGN_LEFT);
        GL11.glDisable(GL_TEXTURE_2D);
    }
}
