package control;

/**
 * Created by strajk on 13.06.15.
 */
public class SteeringLock {
    int timer;

    public SteeringLock() {
        timer = 0;
    }

    public boolean free() {
        if (timer == 0) return true;
        return false;
    }

    public boolean tryTake() {
        if (timer == 0) {
            timer = GameSettings.STEERING_LOCK_TIME;
            return true;
        }
        return false;
    }

    public void update() {
        if (timer > 0) {
            timer--;
        }
    }
}
