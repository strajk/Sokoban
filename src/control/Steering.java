package control;

import commands.Move;
import commands.MovementController;
import gameWorld.GameEngine;
import graphics.Window;
import math.Vec2;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by strajk on 23.05.15.
 */
public class Steering {
    GameEngine gameEngine;
    Window window;
    MovementController movementController;
    private Set<Integer> keysDown = new HashSet<Integer>();
    SteeringLock steeringLock;

    public Steering(GameEngine gameEngine, Window window) {
        this.steeringLock = new SteeringLock();
        this.gameEngine = gameEngine;
        this.window = window;
        this.movementController = new MovementController();
        gameEngine.addLevelObserver(movementController);
    }

    public void update(float delta) {
        steeringLock.update();
        // get keys from buffer and remember or remove
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                switch (Keyboard.getEventKey()) {
                    case Keyboard.KEY_R:
                        gameEngine.levelRestart();
                        movementController.reset();
                    case Keyboard.KEY_Z:
                        if (steeringLock.tryTake()) {
                            movementController.undo();
                        }
                        break;
                    case Keyboard.KEY_PERIOD:
                        break;
                    default:
                        keysDown.remove(Keyboard.getEventKey());
                        break;
                }
                keysDown.add(Keyboard.getEventKey());
            } else {
                switch (Keyboard.getEventKey()) {
                    default:
                        keysDown.remove(Keyboard.getEventKey());
                        break;
                }

            }
        }

        // handle long touch keys
        for (Integer key : keysDown) {
            switch (key) {
                case Keyboard.KEY_LEFT:
                    //gameEngine.gameWorld.movePlayer(new Vec2(-1,0));
                    if (steeringLock.tryTake()) {
                        movementController.execute(new Move(gameEngine.gameWorld, new Vec2(-1, 0)));
                    }
                    break;
                case Keyboard.KEY_RIGHT:
                    // gameEngine.gameWorld.movePlayer(new Vec2(1,0));
                    if (steeringLock.tryTake()) {
                        movementController.execute(new Move(gameEngine.gameWorld, new Vec2(1, 0)));
                    }
                    break;
                case Keyboard.KEY_UP:
                    // gameEngine.gameWorld.movePlayer(new Vec2(0,1));
                    if (steeringLock.tryTake()) {
                        movementController.execute(new Move(gameEngine.gameWorld, new Vec2(0, 1)));
                    }
                    break;
                case Keyboard.KEY_DOWN:
                    //gameEngine.gameWorld.movePlayer(new Vec2(0,-1));
                    if (steeringLock.tryTake()) {
                        movementController.execute(new Move(gameEngine.gameWorld, new Vec2(0, -1)));
                    }
                    break;
            }
        }
    }


    public void pollInput(float delta) {

        update(delta);

        if (Display.isCloseRequested() || isQuitRequested()) {
            window.requestClose();
        }
    }

    public boolean isQuitRequested() {
        return keysDown.contains(Keyboard.KEY_ESCAPE);
    }
}
