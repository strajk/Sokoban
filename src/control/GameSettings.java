package control;

/**
 * Created by strajk on 12.06.15.
 */
public class GameSettings {
    public static String LEVEL_PATH = "levels/level";
    public static String TEXTURES_PATH = "textures/";
    public static Float ELEMENTS_SIZE = 1.0f;
    public static int WIDTH = 800;
    public static int HEIGHT = 800;
    public static int MAX_FPS = 60;
    public static int STEERING_LOCK_TIME = 21;


}
