package control;

import commands.LevelObserver;
import gameWorld.*;
import gameWorld.logicalObjects.*;
import graphics.Background;
import graphics.DisplayController;
import graphics.graphicalObjects.*;
import math.Vec2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by strajk on 13.06.15.
 */
public class ElementsController implements LevelObserver {
    GameEngine gameEngine;
    DisplayController displayController;
    Background background;
    List<BackGroundSquare> backGroundSquares;
    List<WallGraphics> wallGraphicsList;
    List<WorldObjectGraphics> graphicsList;

    public ElementsController(GameEngine gameEngine, DisplayController displayController) {
        this.gameEngine = gameEngine;
        this.displayController = displayController;
        this.background = new Background();
        gameEngine.addLevelObserver(this);
        wallGraphicsList = new ArrayList<WallGraphics>();
    }

    public void updateBackgrouond() {
        wallGraphicsList = new ArrayList<WallGraphics>();
        getObjects();
        backGroundSquares = background.getBackgroundSquares(new Vec2(gameEngine.gameWorld.player.position), wallGraphicsList);
        displayController.setBackground(backGroundSquares);
    }

    public void getObjects() {
        List<WorldObject> displayList;
        displayList = gameEngine.getObjects();
        List<WorldObjectGraphics> displayGraphicsList = new ArrayList<WorldObjectGraphics>();
        for (WorldObject object : displayList) {
            if (object instanceof Wall) {
                WallGraphics wall = new WallGraphics(new Vec2(((Wall) object).position));
                displayGraphicsList.add(wall);
                wallGraphicsList.add(wall);
            } else if (object instanceof Box) {
                displayGraphicsList.add(new BoxGraphics(new Vec2(((Box) object).position)));
            } else if (object instanceof Cross) {
                displayGraphicsList.add(new CrossGraphics(new Vec2(((Cross) object).position), ((Cross) object).filled));
            } else if (object instanceof Player) {
                displayGraphicsList.add(new PlayerGraphics(new Vec2(((Player) object).position)));
            }
        }
        graphicsList = displayGraphicsList;
        displayController.setObjects(displayGraphicsList);
    }

    @Override
    public void levelCompleted() {
        updateBackgrouond();
    }
}
