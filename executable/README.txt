Sokoban project
Author: Artur Ga�uszka

https://gitlab.com/strajk/Sokoban

This is a puzzle game made for "Design Patterns" subject in my 2nd year of university.
It uses OpenGL graphics.

running using jar (recommended):
java -Djava.library.path=windows -jar Sokoban.jar
OR
java -Djava.library.path=linux -jar Sokoban.jar